package services;

import dao.CityDAO;
import dao.FlightDAO;
import entities.City;
import entities.Flight;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class AdminService {
    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public AdminService() {
        flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
    }

    public List<Flight> getFlights() {
        List<Flight> flights = flightDAO.getFlights();
        for(Flight fl : flights) {
            fl.setDepartureCity(cityDAO.getCity(fl.getDepartureCityId()));
            fl.setArrivalCity(cityDAO.getCity(fl.getArrivalCityId()));
        }
        return flights;
    }

    public Flight getFlightById(int id) {
        return flightDAO.findFlight(id);
    }

    public List<City> getCities() {
        return cityDAO.getCities();
    }

    public Flight addFlight(Flight flight) {
        flight.setId(getMaxFlightId());
        return flightDAO.addFlight(flight);
    }

    public Flight updateFlight(Flight flight) {
        return flightDAO.updateFlight(flight);
    }

    public Flight deleteFlight(Flight flight) {
        return flightDAO.deleteFlight(flight);
    }

    private int getMaxFlightId()
    {
        List<Flight> flights = getFlights();
        int id = 0;
        for(Flight fl : flights) {
            if(fl.getId() > id) {
                id = fl.getId();
            }
        }
        return id+1;
    }
}
