package services;

import dao.CityDAO;
import dao.FlightDAO;

import entities.Flight;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class FlightService {
    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public FlightService() {
        flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
    }

    public List<Flight> getFlights() {
        List<Flight> flights = flightDAO.getFlights();
        for(Flight fl : flights) {
            fl.setDepartureCity(cityDAO.getCity(fl.getDepartureCityId()));
            fl.setArrivalCity(cityDAO.getCity(fl.getArrivalCityId()));
        }
        return flights;
    }
}
