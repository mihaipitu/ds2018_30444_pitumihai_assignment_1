package services;

import dao.UserDAO;
import entities.User;
import org.hibernate.cfg.Configuration;

public class LoginService {

    private UserDAO dao;

    public LoginService() {
        dao = new UserDAO(new Configuration().configure().buildSessionFactory());
    }

    public User loginUser(String username,String password) {
        if(username == null || password == null) {
            return null;
        }
        return dao.getUserByUsernameAndPass(username,password);
    }
}
