package servlets;

import entities.Flight;
import services.AdminService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HttpAdminCreateServlet extends HttpServlet {
    private AdminService service = new AdminService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Flight fl = new Flight();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        fl.setFlightNo(req.getParameter("flightNo"));
        fl.setAirplaneType(req.getParameter("airplaneType"));
        fl.setDepartureCityId(Integer.parseInt(req.getParameter("departureCityId")));
        try {
            Date departureTime = sdf.parse(req.getParameter("departureTime"));
            Date arrivalTime = sdf.parse(req.getParameter("arrivalTime"));
            Timestamp timeDeparture = new Timestamp(departureTime.getTime());
            Timestamp timeArrival = new Timestamp(arrivalTime.getTime());
            fl.setDepartureTime(timeDeparture);
            fl.setArrivalTime(timeArrival);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        fl.setArrivalCityId(Integer.parseInt(req.getParameter("arrivalCityId")));

        service.addFlight(fl);
        resp.sendRedirect("/adminRead");
    }
}
