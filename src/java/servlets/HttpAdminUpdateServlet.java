package servlets;

import entities.City;
import entities.Flight;
import services.AdminService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class HttpAdminUpdateServlet extends HttpServlet {

    private AdminService service = new AdminService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Flight flight = service.getFlightById(Integer.parseInt(req.getParameter("flightId")));
        List<City> cities = service.getCities();

        req.setAttribute("flight",flight);
        req.setAttribute("Cities",cities);

        String page = "update.jsp";
        RequestDispatcher dispatcher = req.getRequestDispatcher(page);

        if(dispatcher != null) {
            dispatcher.forward(req,resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Flight flight = new Flight();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        flight.setId(Integer.parseInt(req.getParameter("flightId")));
        flight.setFlightNo(req.getParameter("flightNo"));
        flight.setAirplaneType(req.getParameter("airplaneType"));
        flight.setDepartureCityId(Integer.parseInt(req.getParameter("departureCityId")));
        try {
            Date departureTime = sdf.parse(req.getParameter("departureTime"));
            Date arrivalTime = sdf.parse(req.getParameter("arrivalTime"));
            Timestamp timeDeparture = new Timestamp(departureTime.getTime());
            Timestamp timeArrival = new Timestamp(arrivalTime.getTime());
            flight.setDepartureTime(timeDeparture);
            flight.setArrivalTime(timeArrival);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        flight.setArrivalCityId(Integer.parseInt(req.getParameter("arrivalCityId")));

        service.updateFlight(flight);
        resp.sendRedirect("/adminRead");
    }
}
