package servlets;

import entities.Flight;
import services.FlightService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

public class HttpFlightsServlet extends HttpServlet {
    FlightService service = new FlightService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Flight> flights = service.getFlights();
        String page = "flights.jsp";

        req.setAttribute("Flights",flights);
        RequestDispatcher dispatcher = req.getRequestDispatcher(page);

        if(dispatcher != null) {
            dispatcher.forward(req, resp);
        }
    }
}
