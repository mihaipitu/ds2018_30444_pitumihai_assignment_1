package servlets;

import entities.Flight;
import services.AdminService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HttpAdminDeleteServlet extends HttpServlet {
    private AdminService service = new AdminService();

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Flight fl = service.getFlightById(Integer.parseInt(req.getParameter("flightId")));

        service.deleteFlight(fl);

        resp.sendRedirect("/adminRead");
    }
}
