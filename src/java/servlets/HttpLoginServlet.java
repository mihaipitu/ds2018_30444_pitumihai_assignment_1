package servlets;

import entities.User;
import services.LoginService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;



public class HttpLoginServlet extends HttpServlet {
    private LoginService service = new LoginService();
    // This Method Is Called By The Servlet Container To Process A 'POST' Request.

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        handleRequest(req, resp);
    }

    public void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/html");

        // Post Parameters From The Request
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        User user = service.loginUser(username,password);

        if(user != null) {
            if(user.getType().equals("user")) {
                resp.sendRedirect("/flights");
            }
            if(user.getType().equals("admin")) {
                resp.sendRedirect("/adminRead");
            }
        }
    }
}
