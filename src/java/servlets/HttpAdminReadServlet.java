package servlets;

import entities.City;
import entities.Flight;
import services.AdminService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class HttpAdminReadServlet extends HttpServlet {
    private AdminService service = new AdminService();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Flight> flights = service.getFlights();
        List<City> cities = service.getCities();
        String page = "admin.jsp";

        req.setAttribute("Flights",flights);
        req.setAttribute("Cities",cities);
        RequestDispatcher dispatcher = req.getRequestDispatcher(page);

        if(dispatcher != null) {
            dispatcher.forward(req, resp);
        }
    }
}
