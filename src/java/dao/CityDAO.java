package dao;

import entities.City;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class CityDAO {
    private static final Log LOGGER = LogFactory.getLog(CityDAO.class);

    private SessionFactory factory;

    public CityDAO(SessionFactory factory) {
        this.factory = factory;
    }

    public City addCity(City city) {
        int cityID = -1;
        Session session = factory.openSession();
        Transaction tx =null;
        try
        {
            tx = session.beginTransaction();
            cityID = (Integer) session.save(city);
            city.setId(cityID);
            tx.commit();
        } catch (HibernateException e) {
            if(tx != null) {
                tx.rollback();
            }
            LOGGER.error("",e);
        } finally {
            session.close();
        }
        return city;
    }

    public List<City> getCities(){
        Session session = factory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try
        {
            tx =session.beginTransaction();
            cities = session.createQuery("FROM City").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return cities;
    }

    public City getCity(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try
        {
            tx =session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE id = :id");
            query.setParameter("id",id);
            cities = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0) : null;
    }

}
