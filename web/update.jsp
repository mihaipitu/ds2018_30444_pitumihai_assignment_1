<%@ page import="entities.City" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="entities.Flight" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
  Created by IntelliJ IDEA.
  User: mihai.pitu
  Date: 11/7/2018
  Time: 7:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update Flight</title>
</head>
<body>
    <form id="updateFlightId" method="POST" action="adminUpdate">
        <h>Flight info:</h>
        <br/>
        <div id="flightIdDiv" style="display: none,">
            <span id="airplaneType">Flight Number: </span>
            <%
                Flight flight =(Flight) request.getAttribute("flight");
                out.println("<input type=\"text\" name=\"flightId\" value=\""+flight.getId()+"\"  readonly/>");
            %>
        </div>
        <div id="flightNoDiv">
            <span id="airplaneType">Flight Number: </span>
            <%
                flight =(Flight) request.getAttribute("flight");
                out.println("<input type=\"text\" name=\"flightNo\" value=\""+flight.getFlightNo()+"\"  readonly/>");
            %>
        </div>
        <div id="airplaneTypeDiv">
            <span id="airplaneType">Airplane Type: </span>
            <%
                Flight fl =(Flight) request.getAttribute("flight");
                out.println("<input type=\"text\" name=\"airplaneType\" value=\""+fl.getAirplaneType()+"\" />");
            %>
        </div>
        </div>
        <div id="departureCityDiv">
            <span id="departureCity">Departure City:</span>
            <select name="departureCityId">
                <%
                    List<City> departureCities = (ArrayList<City>) request.getAttribute("Cities");
                    for(City city: departureCities) {
                        out.println("<option value=\"" +city.getId() +"\">"+city.getName()+"<option>");
                    }
                %>
            </select>
        </div>
        <div id="departureTimeDiv">
            <span id="departureTime">Departure Time:</span>
            <%
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

                Flight fl2 =(Flight) request.getAttribute("flight");
                out.println("<input type=\"text\" name=\"departureTime\" value=\""+sdf.format(fl2.getDepartureTime())+"\" />");
            %>
        </div>
        <div id="arrivalCityDiv">
            <span id="arrivalCity">Arrival City:</span>
            <select name="arrivalCityId">
                <%
                    List<City> arrivalCities = (ArrayList<City>) request.getAttribute("Cities");
                    for(City city: arrivalCities) {
                        out.println("<option value=\"" +city.getId() +"\">"+city.getName()+"</option>");
                    }
                %>
            </select>
        </div>
        <div id="arrivalTimeDiv">
            <span id="arrivalTime">Arrival Time:</span>
            <%
                Flight fl3 =(Flight) request.getAttribute("flight");
                out.println("<input type=\"text\" name=\"arrivalTime\" value=\""+sdf.format(fl.getArrivalTime())+"\" />");
             %>
        </div>
        <div id="addFlightBtn">
            <input id="btnAdd" type="submit" value="Add Flight"/>
        </div>
    </form>
</body>
</html>
