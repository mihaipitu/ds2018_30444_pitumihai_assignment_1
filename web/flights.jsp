<%@ page import="entities.Flight" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
  Created by IntelliJ IDEA.
  User: mihai.pitu
  Date: 11/7/2018
  Time: 7:12 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Flights</title>
</head>
<body>
    <table>
        <tr>
            <th>Flight Number</th>
            <th>Airplane Type</th>
            <th>Departure City</th>
            <th>Departure Time</th>
            <th>Arrival City</th>
            <th>Arrival Time</th>
        </tr>
        <%
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
            List<Flight> flights = (ArrayList<Flight>) request.getAttribute("Flights");
            for(Flight flight : flights) {
                out.println("<tr>");
                out.println("<td>"+ flight.getFlightNo() +"</td>");
                out.println("<td>"+ flight.getAirplaneType() +"</td>");
                out.println("<td>"+ flight.getDepartureCity().getName() +"</td>");
                out.println("<td>"+ sdf.format(flight.getDepartureTime()) +"</td>");
                out.println("<td>"+ flight.getArrivalCity().getName() +"</td>");
                out.println("<td>"+ sdf.format(flight.getArrivalTime()) +"</td>");
                out.println("</tr>");
            }
        %>
    </table>
</body>
</html>
