<%@ page import="entities.Flight" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="entities.City" %><%--
  Created by IntelliJ IDEA.
  User: mihai.pitu
  Date: 11/7/2018
  Time: 8:39 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin page</title>
</head>
<body>
<table>
    <tr>
        <th>Flight Number</th>
        <th>Airplane Type</th>
        <th>Departure City</th>
        <th>Departure Time</th>
        <th>Arrival City</th>
        <th>Arrival Time</th>
    </tr>
    <%
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        List<Flight> flights = (ArrayList<Flight>) request.getAttribute("Flights");
        for(Flight flight : flights) {
            out.println("<tr>");
            out.println("<td>"+ flight.getFlightNo() +"</td>");
            out.println("<td>"+ flight.getAirplaneType() +"</td>");
            out.println("<td>"+ flight.getDepartureCity().getName() +"</td>");
            out.println("<td>"+ sdf.format(flight.getDepartureTime()) +"</td>");
            out.println("<td>"+ flight.getArrivalCity().getName() +"</td>");
            out.println("<td>"+ sdf.format(flight.getArrivalTime()) +"</td>");
            out.println("</tr>");
        }
    %>
</table>
<br/>
<br/>
    <form id="addFlightId" name="addFlight" method="POST" action="adminCreate">
        <h>Add a flight:</h>
        <br/>
        <div id="flightNoDiv">
            <span id="flightNo">Flight Number: </span><input type="text" name="flightNo" />
        </div>
        <div id="airplaneTypeDiv">
            <span id="airplaneType">Airplane Type: </span><input type="text" name="airplaneType" />
        </div>
        <div id="departureCityDiv">
            <span id="departureCity">Departure City:</span>
            <select name="departureCityId">
            <%
                List<City> departureCities = (ArrayList<City>) request.getAttribute("Cities");
                for(City city: departureCities) {
                    out.println("<option value=\"" +city.getId() +"\">"+city.getName()+"<option>");
                }
            %>
            </select>
        </div>
        <div id="departureTimeDiv">
            <span id="departureTime">Departure Time:</span><input type="text" name="departureTime"/>
        </div>
        <div id="arrivalCityDiv">
            <span id="arrivalCity">Arrival City:</span>
            <select name="arrivalCityId">
                <%
                    List<City> arrivalCities = (ArrayList<City>) request.getAttribute("Cities");
                    for(City city: arrivalCities) {
                        out.println("<option value=\"" +city.getId() +"\">"+city.getName()+"</option>");
                    }
                %>
            </select>
        </div>
        <div id="arrivalTimeDiv">
            <span id="arrivalTime">Arrival Time:</span><input type="text" name="arrivalTime"/>
        </div>
        <div id="addFlightBtn">
            <input id="btnAdd" type="submit" value="Add Flight"/>
        </div>
    </form>
<br/>
<br/>
    <form id="deleteFlightId" name="deleteflight" method="POST" action="adminDelete">
        <h>Delete a flight:</h>
        <br/>
        <div id="flightsDiv">
            <span id="flight">Flight:</span>
            <select name="flightId">
                <%
                    List<Flight> flightDelete = (ArrayList<Flight>) request.getAttribute("Flights");
                    for(Flight flight : flightDelete) {
                        out.println("<option value=\"" +flight.getId() +"\">"+flight.getFlightNo()+"</option>");
                    }
                %>
            </select>
        </div>
        <div id="deleteFlightBtn">
            <input id="btnDelete" type="submit" value="Delete Flight"/>
        </div>
    </form>
<br/>
<br/>
    <form id="updateFlightId" name="updateflight" method="GET" action="adminUpdate">
        <h>Update a flight:</h>
        <br/>
        <div id="flightDiv">
            <span id="flight2">Flight: </span>
            <select name="flightId">
                <%
                    List<Flight> flightUpdate = (ArrayList<Flight>) request.getAttribute("Flights");
                    for(Flight flight : flightUpdate) {
                        out.println("<option value=\"" +flight.getId() +"\">"+flight.getFlightNo()+"</option>");
                    }
                %>
            </select>
        </div>
        <div id="updateFlightBtn">
            <input id="btnUpdate" type="submit" value="Update flight"/>
        </div>
    </form>
</body>
</html>
